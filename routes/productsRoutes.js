const express = require('express')
const router = express.Router();
const userController = require('./../controllers/userControllers')
const productController = require('./../controllers/productController')
const auth = require('./../auth')
const localToken = require('./../token');
const { route } = require('./userRoutes');
//const { route } = require('./userRoutes');

router.get('/',auth.verify,(req,res) =>{
    productController.getAllProducts().then(result => res.send(result))
})
router.get('/active',auth.verify,(req,res)=>{
    productController.getAllActiveProducts().then(result => res.send(result))
})

router.post('/',auth.verify,(req,res)=>{
    let userData = auth.decode(localToken)
    if(userData.isAdmin){
        productController.addProduct(req.body).then(result => res.send(result))
    }else{
        res.send('Only admin can add products')
    }
})

router.get('/:productId',auth.verify,(req,res)=>{
    productController.getSpecificProduct(req.params.productId).then(result => res.send(result))
})

router.put('/:productId',auth.verify,(req,res)=>{
    let userData = auth.decode(localToken)
    if(userData.isAdmin){
    productController.updateProduct(req.params.productId,req.body).then(result => res.send(result))
    }else{
        res.send('Only admin can update product')
    }
})

router.put('/:productId/archive',auth.verify,(req,res)=>{
    let userData = auth.decode(localToken)
    if(userData.isAdmin){
    productController.archiveProduct(req.params.productId).then(result => res.send(result))
    }else{
        res.send('Only admin can update product')
    }
})

router.put('/:productId/activate',auth.verify,(req,res)=>{
    let userData = auth.decode(localToken)
    if(userData.isAdmin){
    productController.activateProduct(req.params.productId).then(result => res.send(result))
    }else{
        res.send('Only admin can activate product')
    }
})





module.exports = router;