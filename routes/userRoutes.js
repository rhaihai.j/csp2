const express = require('express')
const router = express.Router();
const userController = require('./../controllers/userControllers')
const auth = require('./../auth')
const localToken = require('./../token');
const { reset } = require('nodemon');

router.get('/',auth.verify,(req,res)=>{
    userController.getAllUser().then((result) => res.send(result))
})

router.post('/register',(req,res)=>{
    userController.register(req.body).then((result) => res.send(result))
})

router.post('/login',(req,res)=>{
    userController.login(req.body).then(result => res.send(result))
})

router.get('/view-details',auth.verify,(req,res)=>{
    let userData = auth.decode(localToken)
    userController.viewDetails(userData).then(result => res.send(result))
})

router.get('/userPrev',auth.verify,(req,res)=>{
    let userData = auth.decode(localToken)
        if(userData.isAdmin){
            res.send('User is Admin')
        }else{
            res.send('User is NOT Admin')
        }
 
    
})

// Add Order


// router.post('/addOrder',auth.verify,(req,res) =>{
//     let userData = auth.decode(localToken)
//     userController.addOrder(userData.id,req.body)
// })

// End Add Order

module.exports = router;