const User = require('./../models/User.js');
const Product = require('./../models/Product.js')
const bcrypt = require('bcrypt')
const auth = require('./../auth')

module.exports.getAllProducts = () => {
    return Product.find({}).then((result) =>{
        return result
    })

}

module.exports.getAllActiveProducts = () =>{
    return Product.find({isActive:true}).then((result,error) => {
        if(error){
            return error
        }else{
            return result
        }
    })
}

module.exports.addProduct = (reqBody) => {
    const {name, description, price} = reqBody;

    const newProduct = new Product({
        name:name,
        description:description,
        price:price
    })

    return newProduct.save().then((result,error)=>{
        if(error){
            return false
        }else{
            return result
        }
    })
}

module.exports.getSpecificProduct = (productId) =>{
    return Product.findById({_id:productId}).then((result,error) =>{
        if(error){
            return error
        }else{
            return result
        }
    })
}

module.exports.updateProduct = (productId,reqBody) => {
    return Product.findByIdAndUpdate(productId,reqBody,{new:true}).then((result,error)=>{
        if(error){
            return false
        }else{
            return result
        }
    })
}

module.exports.archiveProduct = (productId) =>{
    return Product.findByIdAndUpdate(productId,{isActive:false},{new:true}).then((result,error)=>{
        if(error){
            return false
        }else{
            return result
        }
    })
}

module.exports.activateProduct = (productId) =>{
    return Product.findByIdAndUpdate(productId,{isActive:true},{new:true}).then((result,error)=>{
        if(error){
            return false
        }else{
            return result
        }
    })

}