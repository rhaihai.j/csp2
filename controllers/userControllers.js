const User = require('./../models/User.js');
const Product = require('./../models/Product');
const bcrypt = require('bcrypt')
const auth = require('./../auth')


module.exports.getAllUser = () => {
    return User.find({}).then((result,error)=>{
        if(error){
            return error
        }else{
            return result
        }
    })
}

module.exports.register = (reqBody) =>{
    const {firstName, lastName, email, password, mobileNo, age} = reqBody;

    const newUser = new User({
        email: email,
        password: bcrypt.hashSync(password,10),
    })

    return newUser.save().then((result,error) => {
        if(result){
            return "Thank you for registering, You may login now with your credentials."
        }else{
            return error
        }
    })
}

module.exports.login = (reqBody) =>{
    const {email, password} = reqBody;
        return User.findOne({email: email}).then((result, error) => {
            if(result == null){
                console.log('No Matching Email found!')
            }else{
                if(bcrypt.compareSync(password,result.password)){
                    return {access:auth.createAccessToken(result)};
                }else{
                    return false;
                }
            }
        })
}

module.exports.viewDetails = (userData) => {

    return User.findOne({email:userData.email}).then((result,error) =>{
        if(error){
            return false;
        }else{
            return result;
        }
    })
}

module.exports.checkUserPrev = (userData) =>{
    return userData.isAdmin
}

// module.exports.addOrder = (userId,reqBody) => {
//     //console.log(`id: ${userId}`)
//     let totalAmount = 0;
//     const arr = reqBody.forEach((element) =>{
//         //console.log(element.quantity)
//        const varia = Product.find({name:element.productName}).then((result,erorr)=>{
//             // console.log(result[0].price + " ### " + element.quantity)
//             // totalAmount = totalAmount + (parseInt(result[0].price) * parseInt(element.quantity))
//             return result[0].price
//         })
//         console.log(varia)
//     })
// }