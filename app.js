const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors')

const userRoutes = require('./routes/userRoutes.js')
const productRoutes = require('./routes/productsRoutes.js')

const app = express();
const port = 3001;




// mongoose.connect('mongodb://localhost/appDB',
// {useNewUrlParser: true, useUnifiedTopology: true})

mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.9s26b.mongodb.net/csp2?retryWrites=true&w=majority', 
	{useNewUrlParser: true, useUnifiedTopology: true}
);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log('Connected to Database'));

app.use(express.urlencoded({extended:true}));
app.use(express.json());
app.use(cors());

app.use('/products',productRoutes)
app.use('/users', userRoutes)

app.listen(port,()=>console.log(`Server running at port: ${port}`));