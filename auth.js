const jwt = require('jsonwebtoken')
const localToken = require('./token')
const secret = "dsajxcsncenjncjlz";

module.exports.createAccessToken = (user) =>{
    const data = {
        id:user._id,
        email:user.email,
        isAdmin:user.isAdmin
    }

    return jwt.sign(data, secret, {});

}

//-------------------Decode -----------------

module.exports.decode = (token) => {
	//let slicedToken = token.slice(7,token.length)
	return jwt.decode(token)
}

//------------------- End Decode ------------

module.exports.verify = (req, res, next) => {

	//where to get the token?
	//let token = req.headers.authorization
    let token = localToken;

	//jwt.verify(token, secret, function)

	if(typeof token !== "undefined"){

		//let slicedToken = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if(err){
				res.send("Authentication failed! Only a verified user can proceed")
			} else {
				next()
			}
		})

	} else {
		res.send(false)
	}


}